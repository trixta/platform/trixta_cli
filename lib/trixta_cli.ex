defmodule TrixtaCLI do
  @moduledoc """
  Documentation for TrixtaCLI.
  """

  @doc """
  Hello world.

  ## Examples

      iex> TrixtaCLI.hello
      :world

  """
  def hello do
    :world
  end
end
